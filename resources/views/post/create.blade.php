@extends('layouts.app')

@section('content')
    <div class="col-lg-10 m-auto">
        <h1 class="h1">Creación de Posts</h1>

        @if(count($errors) > 0)

            @foreach($errors->all() as $error)
                <div class="alert alert-danger">

                    {{$error}}

                </div>

            @endforeach


        @endif

        @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif

        @if(session('error'))
            <div class="alert alert-danger">
                {{session('error')}}
            </div>
        @endif

        {!! Form::open(['action' => 'PostsController@store','method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group">
                {{Form::label('title','Titulo')}}

                {{Form::text('title','',['class' => 'form-control','placeholder' => 'Titulo'])}}
            </div>

            <div class="form-group">
                {{Form::label('body','Texto del Post')}}

                {{Form::textarea('body','',['class' => 'form-control ckeditor','placeholder' => 'Texto del Post'])}}
            </div>

            <div class="form-group">
                {{Form::file('cover_image')}}
            </div>

            {{Form::submit('Crear',['class'=>'btn btn-primary'])}}

        {!! Form::close() !!}

    </div>
@endsection
