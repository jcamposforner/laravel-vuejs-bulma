@extends('layouts.app')

@section('content')
    <div class="row m-auto">
    <div class="col-lg-10 ml-2 py-4">

        @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        @endif

        @if(session('error'))
            <div class="alert alert-danger">
                {{session('error')}}
            </div>
        @endif
        <h1 class="h1">Panel de Posts</h1>

        @if(count($posts) > 0)
            @foreach($posts as $post)
                <div class="card my-4">
                    <img src="/storage/cover_images/{{$post->cover_image}}" style="width: auto" alt="card-image-{{$post->id}}" class="card-img-top">
                    <div class="card-body">
                        <h2 class="h2">{{$post->title}}</h2>
                        <p class="card-text">
                            {!! $post->body !!}
                        </p>
                        <div class="py-4">
                        <a href="/admin/posts/{{$post->id}}/edit" class="btn btn-primary float-left">Editar</a>
                        {!! Form::open(['action' => ['PostsController@destroy',$post->id],'method' => 'POST','class' => 'pull-right']) !!}

                            {{Form::hidden('_method','DELETE')}}
                            {{Form::submit('Borrar',['class'=>'btn btn-danger'])}}

                        {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            @endforeach
            {{$posts->links()}}
        @else
            <p>No hay posts aún</p>
        @endif
    </div>
    <div class="col-lg-2 py-4 position-fixed" style="right: 0">
        <h1 class="h1" style="opacity: 0;">Menu</h1>
        <ul class="my-4">
            <a href="/admin/posts/create" class="col-lg-12 btn btn-success"><li>Crear Post</li></a>
        </ul>
    </div>
    </div>
@endsection
