import ExampleComponent from './components/ExampleComponent';
import SobreMi from './components/SobreMi';
import NotFound from './components/NotFound';
import VueRouter from 'vue-router';
import Contact from './components/Contact';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: ExampleComponent
        },
        {
            path: '/about',
            name: 'about',
            component: SobreMi
        },
        {
            path: '/contacto',
            name: 'contacto',
            component: Contact
        },
        // {
        //     path: '/404',
        //     component: NotFound
        // },
        // {
        //     path: '*',
        //     redirect: '/404'
        // }
    ],
});

export default router;