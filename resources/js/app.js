import Vue from 'vue';
import VueRouter from 'vue-router';
import router from './routes';

import App from './components/App';

require('../sass/app.scss');

// Componentes
Vue.component('columns-component',require('./components/Partials/Columns'));
Vue.component('form-email-component',require('./components/Partials/FormEmail'));
Vue.component('footer-component',require('./components/Partials/Footer'));


Vue.use(VueRouter);


require('./assets/sass/main.scss');

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
