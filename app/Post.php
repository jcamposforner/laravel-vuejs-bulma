<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // Table Name
    protected $table = 'posts';
    // Primary key
    public $primaryKey = 'id';
    //Timestapms
    public $timestamps = true;

    public function comments()
    {
        return $this->hasMany(Comments::class);
    }
}
