<?php

namespace App\Http\Controllers;

use App\Post;

class PostController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // DEVUELVE USER AUTENTICADO
//        $user = Auth::user();
//        return $user;

        // Paginador

        $post = Post::orderBy('title','desc')->paginate(1);
        return $post;
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        if(!$post){
            return \response()->json(['status'=>'404'],404);
        }
        return $post;
    }

}
